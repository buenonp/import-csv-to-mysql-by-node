#!/usr/bin/env node

'use strict';

// Bibliotecas necessárias. Instalar com `npm install` via linha de comando.
const parse      = require('csv-parse');
const util       = require('util');
const fs         = require('fs');
const mysql      = require('mysql');
const async      = require('async');
const csvHeaders = require('csv-headers');

// Configuração do banco de dados.
const dbhost = process.argv[2]; // Host de consumo do mysql sem a porta. Ex: `localhost`. Porta default: 3306;
const dbuser = process.argv[3]; // Usuário de acesso ao banco mysql;
const dbpass = process.argv[4]; // Senha de acesso ao banco mysql;
const dbname = process.argv[5]; // Nome do banco de dados onde será inserida a tabela do arquivo .csv;
const tblnm  = process.argv[6]; // Nome da tabela onde serão inseridos os dados do arquivo csv;
const csvfn  = process.argv[7]; // Caminho do arquivo .csv.

// Configuração da utilização do arquivo .csv.
new Promise((resolve, reject) => {
    csvHeaders({
        file      : csvfn,
        delimiter : ','
    }, function(err, headers) {
        if (err) reject(err);
        else resolve({ headers });
    });
})

// Conexão  com o banco de dados mysql.
.then(context => {
    return new Promise((resolve, reject) => {

        context.db = mysql.createConnection({
            host     : dbhost,
            user     : dbuser,
            password : dbpass,
            database : dbname
        });

        context.db.connect((err) => {
            if (err) {
                console.error('error connecting: ' + err.stack);
                reject(err);
            } else {
                resolve(context);
            }
        });
    })
})

// Exclusão da tabela configurada caso ela já exista.
.then(context => {
    return new Promise((resolve, reject) => {
        context.db.query(`DROP TABLE IF EXISTS ${tblnm}`,
        [ ],
        err => {
            if (err) reject(err);
            else resolve(context);
        })
    });
})

// Resgate dos dados do csv e criação da tabela configurada caso ela ainda não exista.
.then(context => {
    return new Promise((resolve, reject) => {
        var fields = '';
        var fieldnms = '';
        var qs = '';
        context.headers.forEach(hdr => {
            hdr = hdr.replace(' ', '_');
            if (fields !== '') fields += ',';
            if (fieldnms !== '') fieldnms += ','
            if (qs !== '') qs += ',';
            fields += ` ${hdr} TEXT`;
            fieldnms += ` ${hdr}`;
            qs += ' ?';
        });
        context.qs = qs;
        context.fieldnms = fieldnms;
        context.db.query(`CREATE TABLE IF NOT EXISTS ${tblnm} ( ${fields} )`,
        [ ],
        err => {
            if (err) reject(err);
            else resolve(context);
        })
    });
})

// Inserção dos dados do arquivo .csv na tabela do banco de dados mysql.
.then(context => {
    return new Promise((resolve, reject) => {
        fs.createReadStream(csvfn).pipe(parse({
            delimiter: ',',
            columns: true,
            relax_column_count: true
        }, (err, data) => {
            if (err) return reject(err);
            async.eachSeries(data, (datum, next) => {
                var d = [];
                try {
                    context.headers.forEach(hdr => {
                        d.push(datum[hdr]);
                    });
                } catch (e) {
                    console.error(e.stack);
                }
                if (d.length > 0) {
                    context.db.query(`INSERT INTO ${tblnm} ( ${context.fieldnms} ) VALUES ( ${context.qs} )`, d, //Query principal de inserção na tabela.
                    err => {
                        if (err) { console.error(err); next(err); }
                        else setTimeout(() => { next(); });
                    });
                } else { console.log(`empty row ${util.inspect(datum)} ${util.inspect(d)}`); next(); }
            },
            err => {
                if (err) reject(err);
                else resolve(context);
            });
        }));
    });
})

.then(context => { context.db.end(); })
.catch(err => { console.error(err.stack); });